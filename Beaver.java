public class Beaver {

		public String name;
		public String province;
		public int age;	
		
		
		public void sayHi(String name){
			
			System.out.println("Hello " + name + " the beaver!");
			
		}
		
		public void buildDam(String name) {
			
			System.out.println(name + " built a dam!");
		
		}
}