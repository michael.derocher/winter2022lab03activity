public class Application {

	public static void main(String[] args) {
		
		Beaver B1 = new Beaver();
		
		B1.name = "Bucktooth Bill";
		B1.province = "Saskatchewan";
		B1.age = 7;
		
		Beaver B2 = new Beaver();
		
		B2.name = "Reginald";
		B2.province = "British-Colombia";
		B2.age = 11;
		
		
		System.out.println(B1.name + " " + B1.province + " " + B1.age);
		System.out.println(B2.name + " " + B2.province + " " + B2.age);
		
		B1.sayHi(B1.name);
		B2.sayHi(B2.name);
		
		Beaver[] colony = new Beaver[3];
		
		colony[0] = B1;
		colony[1] = B2;
		colony[2] = new Beaver();
		
		colony[2].name = "Ptit Jean";
		colony[2].province = "Quebec";
		colony[2].age = 3;
		
		System.out.println(colony[2].name + " " + colony[2].province + " " + colony[2].age);
	}



}